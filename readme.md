# Kata-exemple

## Présentation

Ce projet est un template de projet WinDev permettant de démarrer rapidement un nouveau projet basé sur le framework de test [wxUnit](https://gitlab.com/Johjo/wxunit). Il est paramétré pour lancer automatiquement les tests et afficher le résultat dans wxUnitViewer.

## Pré-requis
wxUnitViewer doit être installé et configuré sur le poste. Pour cela, vous pouvez lire la [description de wxUnitViewer](https://gitlab.com/Johjo/wxunitviewer).

## Installation
Vous pouvez cloner le projet ou bien le [télécharger au format zip](https://gitlab.com/Johjo/kataexemple25-windev/-/archive/master/kataexemple25-windev-master.zip)

Ensuite, vous pouvez renommer le répertoire du projet ainsi que le fichier KataExemple.wdp avec le nom de votre choix.

Continuez en ouvrant le projet avec WinDev.

Enfin, executez les tests en faisant un GO du projet (Ctrl + F9) et vérifiez que wxUnitViewer s'affiche correctement avec un test qui réussit et un test qui échoue.









